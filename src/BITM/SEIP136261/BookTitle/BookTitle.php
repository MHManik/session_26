<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{
    public  $id;
    public $bookTitle;
    public $authorName;

    public function __construct()
    {
        parent::__construct();
        if(isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=Null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("book_title",$postVariableData)){
            $this->bookTitle=$postVariableData["book_title"];
        }
        if(array_key_exists("author_name",$postVariableData)){
            $this->authorName=$postVariableData['author_name'];
        }
    }

    public function store(){
        $arrData=array($this->bookTitle,$this->authorName);
        $sql="insert into book_title(book_title,author_name) VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result) {

            Message::Message("Data has been inserted successfully :)");
        }
        else{
            Message::Message("Failed! Data has not been inserted successfully :(");

        }
        Utility::redirect('create.php');
    }

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title ORDER BY book_title DESC ');



        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function delete($id){

      return $id;
    }


}