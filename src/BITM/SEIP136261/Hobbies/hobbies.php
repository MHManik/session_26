<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Hobbies extends DB{


    public $id;
    public $name="user";
    public $hobbies="";

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('name',$data)){


            $this->name= $data['name'];

        }
        if(array_key_exists('hobbies',$data)){

            $this->hobbies = implode(', ', $data['hobbies']);
        }

    }
    public function store(){
        $arrData = array($this->name,$this->hobbies);
        $sql = "INSERT INTO hobbies(name,hobbies) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }

    public function index(){


    }

}